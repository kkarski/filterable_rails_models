class ApplicationRecord < ActiveRecord::Base
  include Filterable

  POSTGRES_INT_MAX     = 2_147_483_647
  POSTGRES_BIG_INT_MAX = 9_223_372_036_854_775_807

  self.abstract_class = true
  self.belongs_to_required_by_default = true

  def self.associations
    self.reflect_on_all_associations.inject({}) { |result, relation|
      result[relation.name] = relation.class_name
      result
    }.freeze
  end
end

