module Filterable
  extend ActiveSupport::Concern

  included do
    def self.filter(search_term, columns)
      return where(nil) if search_term.blank? || columns.blank?

      composed = nil
      matched_relations = []
      arel_table_object = arel_table
      model_associations = associations

      columns.each do |column|
        matching_relation = model_associations.keys.select { |key|
          column.start_with?(key.to_s)
        }.first

        if matching_relation.present?
          relation_arel_table_object = model_associations[matching_relation]
                                         .constantize
                                         .arel_table

          matched_relations << matching_relation
        else
          relation_arel_table_object = nil
        end

        if relation_arel_table_object.nil?
          to = arel_table_object
        else
          to = relation_arel_table_object

          column.slice!("#{matching_relation}_")
        end

        if composed.nil?
          composed = column_ilike(to, column, search_term)
        else
          composed = composed.or(column_ilike(to, column, search_term))
        end
      end

      qry = where(nil)

      if matched_relations.any?
        matched_relations.uniq.each do |relation|
          qry = qry.joins(relation)
        end
      end

      qry.where(composed)
    end

    # generate CAST( table.column as TEXT ) ILIKE '%search_term%' query
    def self.column_ilike(arel_table_object, column, search_term)
      Arel::Nodes::NamedFunction.new(
        'CAST',
        [arel_table_object[column.to_sym].as('TEXT')]
      ).matches("%#{escape_wildcards(search_term)}%")
    end

    # replace % \ to \% \\
    def self.escape_wildcards(unescaped)
      case ActiveRecord::Base.connection.adapter_name
      when 'Mysql2'.freeze, 'PostgreSQL'.freeze
        # Necessary for PostgreSQL and MySQL
        unescaped.to_s.gsub(/([\\|%_.])/, '\\\\\\1')
      else
        unescaped
      end
    end
  end
end

